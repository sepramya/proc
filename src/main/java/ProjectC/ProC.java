package ProjectC;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import utils.DataLibrary;

public class ProC {

	public static String excelFileName ;

	@BeforeTest
	public void setData() {
		
		excelFileName = "Proc";
	}
	@DataProvider(name = "fetchData")
	public Object[][] fetchData() throws IOException {
		return DataLibrary.readExcelData(excelFileName);
	}
	@Test (dataProvider="fetchData")
	
	public void Project(String email, String pwd)
	{

		//Launch chrome browser
		System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		//Maximize window
		driver.manage().window().maximize();
		//Launch URL
		driver.navigate().to("https://acme-test.uipath.com/account/login");
		//Enter Email
		driver.findElementById("email").sendKeys(email);
		//Enter Password
		driver.findElementById("password").sendKeys(pwd);
		//Click Login
		driver.findElementById("buttonLogin").click();

		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		//Mouse over on action
		Actions mouseover = new Actions(driver);
		mouseover.moveToElement(driver.findElementByXPath("//*[@id='dashmenu']/div[5]/button")).pause(2000).perform();
		//Select search for Vendors
		driver.findElementByLinkText("Search for Vendor").click();
		//Enter Tax Id
		driver.findElementById("vendorTaxID").sendKeys("FR121212");
		//Click Search
		driver.findElementById("buttonSearch").click();
		//Print the Vendor Name
		String text = driver.findElementByXPath("//table/tbody/tr/td[2]").getText();				
		System.out.println(text);
		//close the browser
		driver.close();
	}

	
	}	



















